package Premier;

import Automate.Automate;
import Automate.Etat;
import Automate.Transition;

import java.util.ArrayList;
import java.util.HashSet;

public class Premier {
    
    private Automate automate;
    private ArrayList<Etat> listeNonTerminaux;
    private HashSet<String> listeTermiaux;
    private HashSet<String> resultat;

    public Premier(Automate automate) {
        this.automate = automate;
        this.listeNonTerminaux = new ArrayList<Etat>();
        this.listeNonTerminaux.addAll(this.automate.getListeEtats());
        this.listeTermiaux = new HashSet<String>();
        for (Etat etat: this.automate.getListeEtats()) {
            for (Transition transition :etat.getListeTransition()) {
                this.listeTermiaux.addAll(transition.getCaractereReconnu());
            }
        }
        this.resultat= new HashSet<String>();
    }

    public void premiereRegle(String X){
        if (this.listeTermiaux.contains(X)){
            this.resultat.add(X);
        }
    }

    public void deuxiemeRegle(Etat X){
        for (Transition transition:X.getListeTransition()) {
            if (transition.getCaractereReconnu().equals("€") && transition.getFin().getEtatFinal()){
                this.resultat.add("€");
            }
        }
    }

    public void troisiemeRegle(Etat X){
        for (Transition transision : X.getListeTransition()) {
            for (int i=0; i<transision.getCaractereReconnu().size(); i++){
                if (this.listeTermiaux.contains(transision.getCaractereReconnu().get(0))){
                    this.premiereRegle(transision.getCaractereReconnu().get(0));
                }else{
                    this.deuxiemeRegle(transision.getFin());
                }
                //TODO: continuer la suite de la règle
            }
        }
    }



}

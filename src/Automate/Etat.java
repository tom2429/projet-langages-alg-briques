package Automate;

import java.util.ArrayList;

public class Etat {

    private String nom;
    private Boolean etatFinal;
    private ArrayList<Transition> listeTransition;

    /**
     * Constructeur d'un Etat
     * @param nom, une String qui représente le nom de l'état
     * @param etatFinal, un Boolean qui nous informe si l'état est final ou non
     */
    public Etat(String nom, Boolean etatFinal) {
        this.listeTransition = new ArrayList<>();
        this.nom = nom;
        this.etatFinal = etatFinal;
    }

    /**
     * Permet d'ajouter une transition à un état
     * @param transition, une Transition que l'on ajoute à la liste des transition de l'état
     */
    public void addTransition(Transition transition){
        this.listeTransition.add(transition);
    }

    /**
     * Getter qui nous permet de savoir si l'état est final
     * @return un Boolean
     */
    public Boolean getEtatFinal() {
        return etatFinal;
    }

    /**
     * Getter qui nous permet de récupérer la liste des transitions d'un état
     * @return une ArrayList<Transition>
     */
    public ArrayList<Transition> getListeTransition() {
        return listeTransition;
    }

    /**
     * Permet de savoir si un mot est dans une des transition d'un état
     * @param mot, une String
     * @return une Transition qui contient le le mot rechercher, ou null.
     */
    public Transition estDansUneTransition(String mot){
        for (Transition transition : this.getListeTransition()){
            if (transition.estDansTransition(mot)){
              return transition;
            }
        }
        return null;
    }
}

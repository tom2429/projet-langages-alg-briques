package Automate;

import java.io.File;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {
        AnalyseLexicale specificationLexicale = new AnalyseLexicale();
        try {
            String s = specificationLexicale.remplacer(new File(args[0]));
            System.out.println(s);
        } catch (FileNotFoundException e) {
            System.out.println("fichier non trouvé");
        }
    }
}
package Automate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AnalyseLexicale {

    private Automate automateEntier;
    private Automate automateIndent;

    public AnalyseLexicale() {

        /*====================== AUTOMATE ENTIER ===========================*/

        Etat etat0Entier = new Etat("0", false);
        Etat etat1Entier = new Etat("1", true);
        Etat etat2Entier = new Etat("2", false);
        Etat etat3Entier = new Etat("3", true);

        Transition transition0Entier = new Transition(etat0Entier,etat1Entier);
        transition0Entier.setCaractereReconnu("0");

        Transition transition1Entier = new Transition(etat0Entier,etat2Entier);
        transition1Entier.setCaractereReconnu("-");

        Transition transition2Entier = new Transition(etat0Entier,etat3Entier);
        transition2Entier.setCaractereReconnu("1","2","3","4","5","6","7","8","9");

        Transition transition4Entier = new Transition(etat2Entier,etat3Entier);
        transition4Entier.setCaractereReconnu("1","2","3","4","5","6","7","8","9");

        Transition transition3Entier = new Transition(etat3Entier,etat3Entier);
        transition3Entier.setCaractereReconnu("0","1","2","3","4","5","6","7","8","9");

        etat0Entier.addTransition(transition0Entier);
        etat0Entier.addTransition(transition1Entier);
        etat0Entier.addTransition(transition2Entier);
        etat2Entier.addTransition(transition4Entier);
        etat3Entier.addTransition(transition3Entier);

        this.automateEntier = new Automate(etat0Entier);
        automateEntier.addListeEtats(etat0Entier);
        automateEntier.addListeEtats(etat1Entier);
        automateEntier.addListeEtats(etat2Entier);
        automateEntier.addListeEtats(etat3Entier);

        //====================== AUTOMATE IDENTIFICATEUR ===========================

        Etat etat0Identificateur = new Etat("0", false);
        Etat etat1Identificateur = new Etat("1", true);

        Transition transition0Identificateur = new Transition(etat0Identificateur,etat1Identificateur);
        transition0Identificateur.setCaractereReconnu("a","b","c","d","e","f","g","e","f","g"
                ,"h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");

        Transition transition1Identificateur = new Transition(etat1Identificateur,etat1Identificateur);
        transition1Identificateur.setCaractereReconnu("0","1","2","3","4","5","6","7"
                ,"8","9","a","b","c","d","e","f","g","e","f","g","h","i","j",
                "k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");

        etat0Identificateur.addTransition(transition0Identificateur);
        etat1Identificateur.addTransition(transition1Identificateur);

        this.automateIndent = new Automate(etat0Identificateur);
        automateIndent.addListeEtats(etat0Identificateur);
        automateIndent.addListeEtats(etat1Identificateur);
        automateIndent.addMotCle("program","end","break",
                "begin","while","for","if","do","true","false","form","not",
                "then","else","and","or");
    }

    /**
     * Permet de lire un fichier et de l'analyser en fonction des 2 automates precedents.
     * Elle permet également de remplacer des thermes si le mot analyser est reconnu par tel ou tel automate.
     * @param algo1, un Fichier qui est écrit en AGL01
     * @return, une String qui est le resultat de l'analyser lexical via les 2 automates
     * @throws FileNotFoundException
     */
    public String remplacer(File algo1) throws FileNotFoundException {
        Scanner fichier =  new Scanner(algo1);
        StringBuilder res = new StringBuilder();
        String ligne ;
        while (fichier.hasNextLine()){
            ligne = fichier.nextLine();
            ligne = ligne.replaceAll(";", " ;");
            String [] mots = ligne.strip().split(" ");
            for (String mot: mots) {
                if (this.automateIndent.reconnaissanceMot(mot).equals("valideMotCle")) {
                    res.append(" ").append(mot);
                }else if (this.automateIndent.reconnaissanceMot(mot).equals("valideEtatFinal")) {
                    res.append(" ident");
                }else if (this.automateEntier.reconnaissanceMot(mot).equals("valideEtatFinal")) {
                    res.append(" entier");
                }else {
                    if (mot.equals(";"))
                        res.append(mot);
                    else res.append(" ").append(mot);
                }
            }
        }
        return res.toString().trim();
    }
}

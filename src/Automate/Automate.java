package Automate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class Automate {

    private ArrayList<Etat> listeEtats;
    private Etat etatInit;
    private ArrayList<String> motsCles;

    /**
     * Constructeur de l'automate
     * @param etatCourrant, l'état initial/ de départ de l'automate
     */
    public Automate(Etat etatCourrant) {
        this.motsCles = new ArrayList<>();
        this.listeEtats = new ArrayList<>();
        this.etatInit = etatCourrant;
    }

    /**
     * Permet d'ajouter des mots clés que reconnaitera l'automate
     * @param mots, liste de mots clés
     */
    public void addMotCle(String... mots) {
        this.motsCles.addAll(Arrays.asList(mots));
    }

    /**
     * Permet d'ajouter des états à l'automates
     * @param etat, un état à ajouter
     */
    public void addListeEtats(Etat etat){
        this.listeEtats.add(etat);
    }

    /**
     * Getter de l'attribut listeEtats
     * @return une ArrayList<Etat>
     */
    public ArrayList<Etat> getListeEtats() {
        return listeEtats;
    }

    /**
     * Permet de découper une phrase en une liste contenant tous les mots de la phrase
     * @param mot, un string représentant une phrase
     * @return une ArrayList<String>, qui contient tous les mots de la chaine de caractère passer
     * en parramètre.
     */
    public ArrayList<String> decoupeMot(String mot){
        ArrayList<String> listeCaractere = new ArrayList<>();
        for (int i=0; i<mot.length(); i++){
            listeCaractere.add(mot.substring(i,i+1));
        }
        //System.out.println(listeCaractere);
        return listeCaractere;
    }

    /**
     * Permet de reconnaitre un mot fesant parti de l'automate.
     * @param mot, une String qui est une chaine de caractère que l'on souhaite tester
     * @return une String, nous disant si à la fin de la chaine, nous sommes bien dans un état final
     * et donc que la chaine en entrée est reconnu par l'automate
     */
    public String reconnaissanceMot(String mot){
        Etat etatCourrant = this.etatInit;
        if (!this.motsCles.contains(mot)){
            ArrayList<String> listeCaractere = decoupeMot(mot);
            for (String caractere : listeCaractere) {
                if (!Objects.isNull(etatCourrant.estDansUneTransition(caractere))){
                    //System.out.println(etatCourrant.getNom());
                    etatCourrant = etatCourrant.estDansUneTransition(caractere).getFin();
                    //System.out.println(etatCourrant.getNom());
                }else{
                    return "invalideTransition";
                }
            }
            if (etatCourrant.getEtatFinal()){
                return "valideEtatFinal";
            }else{
                return  "invalideEtatFinal";
            }
        }else{
            return "valideMotCle";
        }
    }
}

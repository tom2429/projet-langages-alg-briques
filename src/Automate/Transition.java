package Automate;

import java.util.ArrayList;
import java.util.Arrays;

public class Transition {

    private Etat debut;
    private Etat fin;
    private ArrayList<String> caractereReconnu;

    /**
     * Constructeur d'une Transition
     * @param debut, un Etat qui est l'état de départ de la transition
     * @param fin, un Etat qui est l'état de fin de la transition
     */
    public Transition(Etat debut, Etat fin) {
        this.caractereReconnu = new ArrayList<>();
        this.debut = debut;
        this.fin = fin;
    }

    /**
     * Getter qui permet de récupérer l'état de fin d'une transition
     * @return un Etat
     */
    public Etat getFin() {
        return fin;
    }

    /**
     * Getter qui permet de recupérer la liste des caractères reconnus par la transition
     * @return une ArrayList<String>
     */
    public ArrayList<String> getCaractereReconnu() {
        return caractereReconnu;
    }

    /**
     * Setter qui nous permet de remplir la liste des caractères reconnus d'une transition
     * @param caracteres
     */
    public void setCaractereReconnu(String... caracteres) {
        this.caractereReconnu.addAll(Arrays.asList(caracteres));
    }

    /**
     * Permet de savoir si un mot est dans la liste des caractères reconnus d'une transition
     * @param mot, une String
     * @return soit true si le mot est dans les caractères reconnus de la transition, sinon false
     */
    public boolean estDansTransition(String mot){
        for (String caractere : this.caractereReconnu){
            if (caractere.equals(mot)){
                return true;
            }
        }
        return false;
    }
}

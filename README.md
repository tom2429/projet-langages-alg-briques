# Projet-langage-algebrique
 
 Pour lancer mon Projet, il faudra que vous fassiez les commandes suivantes
 depuis la racine du projet :
 
 ```bash
cd out/artifacts/projet_langages_alg_briques_jar/
java -jar projet-langages-alg-briques.jar ../../../src/exemple/Exemple1
```

Vous pouvez tester avec les Exemples de 1 à 6.
 
 
#Choix de développement

Analyse Lexicale : 

Tout d'abord, j'ai crée des classes Automate, Etat et Transition, pour conceptualiser mon Automate.

Puis, j'ai fais en sorte que un Etat possède une liste de Transition, qui elle même contient une liste
de caractères et également un automate qui soit, lui même composé d'Etat.
J'ai aussi fait en sorte que, pour un automate, on puisse lui ajouter des mots clés qui seront directement
reconnus par l'automate en question.

Ensuite, j'ai créer ma classe AnalyseLexicale et j'ai créé mes 2 Automates, puis je les ai testé.

Pour finir, j'ai créé ma classe Main, ou j'ai mis en place le système de passage en paramètre d'un fichier
écrit en ALG01.


D'ailleurs voici le git sur lequel, j'ai fais le projet : https://gitlab.com/tom2429/projet-langages-alg-briques